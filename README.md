Project dalam grub "Binar-FSW-std" merupakan project untuk menyelesaikan challenge pada setiap chapter di Binar Academy<br>
Program ini merupakan Bootcamp Fullstack Web Developer (FSW) wave 22<br>
Project pertama dimulai pada chapter ke 3

# Chapter-3
Project chapter 3 merupakan project awal bootcamp FSW 22 yang dimulai pada pembahasan materi front-end<br>
Chapter ini mengenalkan cara styling dan layouting pada web<br>
Tugas pada challenge ini adalah membuat halaman website berdasarkan kriteria yang disediakan<br>
Pada challenge ini menggunakan *Bootstrap versi 4.0*<br>

# File git lokasi
Berada di branch master<br>
Sekian<br>
Terima kasih
